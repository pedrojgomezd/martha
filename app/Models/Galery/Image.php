<?php

namespace App\Models\Galery;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $fillable = ['url'];
    /**
     * Get all of the posts that are assigned this tag.
     */
    public function products()
    {
        return $this->morphedByMany(\App\Models\Inventory\Product::class, 'imggable');
    }
}
