<?php

namespace App\Models\Inventory;

use App\Provider;
use App\Models\Config\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name', 'bar_code', 
                           'description', 'reference', 
                           'initial_amount', 'quantity_sold', 
                           'cost', 'provider_id', 
                           'sub_category_id', 'country', 'material'];

    public function articles()
    {
    	return $this->hasMany(Article::class);
    }

    public function prices()
    {
    	return $this->belongsToMany(Price::class)
    				->withPivot('amount');
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function sub_category()
    {
        return $this->belongsTo(\App\Models\Config\SubCategory::class);
    }

    /**
     * Get all of the tags for the post.
     */
    public function imgs()
    {
        return $this->morphToMany(\App\Models\Galery\Image::class, 'imggable');
    }

    public function colors()
    {
        return $this->hasMany(Color::class);
    }
}
