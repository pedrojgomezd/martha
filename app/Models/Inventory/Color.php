<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = ['description', 'hex'];
}
