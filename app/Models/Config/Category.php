<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name', 'slug', 'order'];

    public function sub_categories()
    {
        return $this->hasMany(SubCategory::class);
    }


    public function products()
    {
        return $this->hasManyThrough(\App\Models\Inventory\Product::class, SubCategory::class);
    }
}
