<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_product';

    protected $fillable = ['order_id', 'product_id', 'article_id','quantity'];

    
    public function article()
    {
        return $this->hasOne(\App\Models\Inventory\Article::class);
    }
}
