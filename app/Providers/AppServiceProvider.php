<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'products' => \App\Models\Inventory\Product::class,
        ]);

        Blade::component('commerce.components.products.products-list', 'products');
        Blade::component('commerce.components.products.product', 'product');
        $this->headerWithCategory();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function headerWithCategory()
    {
        View::composer(
            ['commerce.components.headers.header-full',
            'commerce.components.headers.header-mobil',
            'commerce.components.category.sidebar'],
            function($view) {
                $view->withCategories(
                    (new \App\Repositories\Categories)->allCategoryWithSubCategory()
                );
            }
        );
    }
}
