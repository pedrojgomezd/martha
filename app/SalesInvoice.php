<?php

namespace App;

use App\Client;
use App\Models\Inventory\Article;
use App\Models\Inventory\InventoryItem;
use Illuminate\Database\Eloquent\Model;

class SalesInvoice extends Model
{
    protected $fillable = ['expires', 'status', 'total','paid', 'price_id'];

    public function client()
    {
    	return $this->belongsTo(Client::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class)
    				->withPivot('price', 'quantity', 'total');
    }

    public function addArticles($articles)
    {
    	
        foreach ($articles as $article) {
            $this->articles()->attach($article['article'], $article['pivot']);
        }

        return $this;

    }
}
