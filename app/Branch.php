<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name', 'address', 'representative', 'phone', 'email'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
