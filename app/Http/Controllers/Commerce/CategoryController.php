<?php

namespace App\Http\Controllers\Commerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config\Category;
use App\Models\Config\SubCategory;

class CategoryController extends Controller
{
    public function __invoke($category, $sub_category = null)
    {
        $sub_category ? 
        $products = SubCategory::where('slug', $sub_category)->with('products')->first()
        : $products = Category::where('slug', $category)->with('products')->first();
        // dd($products->toArray());
        return view('commerce.pages.category')->withCategory($products);
    }
}
