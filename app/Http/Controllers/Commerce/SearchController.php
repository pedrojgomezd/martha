<?php

namespace App\Http\Controllers\Commerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config\Category;
use App\Models\Inventory\Product;

class SearchController extends Controller
{
    public function __invoke()
    {
        $query = request('query');
        $products = Product::where('name', 'like', "%{$query}%")->get();//->with('products')->first();
        return view('commerce.pages.search')->withProducts( $products);
    }
}
