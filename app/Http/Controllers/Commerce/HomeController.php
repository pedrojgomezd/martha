<?php

namespace App\Http\Controllers\Commerce;

use App\Repositories\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public $products;

    public function __construct()
    {
        $this->products = new Products;
    }

    public function __invoke()
    {
        return view('commerce.pages.home')
                ->with(
                    [
                        'suggestions' => $this->products->getSuggestions(),
                        'recients' => $this->products->getRecients()
                    ]
                );
    }
}
