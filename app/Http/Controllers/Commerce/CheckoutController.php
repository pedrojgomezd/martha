<?php

namespace App\Http\Controllers\Commerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Inventory\Article;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('commerce.pages.cart');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content = \Cart::getContent()->map(function($item) {
            return "{$item->name}, {$item->quantity}";
        })->values()->toJson();

        $order = $this->addToOrdersTable($request, null);

        return $order;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function addToOrdersTable($request, $error)
    {
        // Insert into orders table
        $order = Order::create([
            'user_id' => auth()->user() ? auth()->user()->id : null,
            'billing_email' => $request->billing_email,
            'billing_name' => "{$request->billing_first_name} {$request->billing_last_name}",
            'billing_address' => "{$request->billing_address_1} {$request->billing_address_2} ",
            'billing_city' => $request->billing_city,
            'billing_province' => $request->billing_state,
            'billing_postalcode' => $request->billing_postcode,
            'billing_phone' => $request->billing_phone,
            'billing_name_on_card' => $request->name_on_card ? $request->name_on_card : '',
            'billing_discount' => 0,
            'billing_discount_code' => 0,
            'billing_subtotal' => \Cart::getSubTotal(),
            'billing_tax' => 0,
            'billing_total' => \Cart::getTotal(),
            'error' => $error,
        ]);

        // Insert into order_product table
        foreach (\Cart::getContent() as $item) {
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item->id,
                'article_id' => $item->attributes->id,
                'quantity' => $item->quantity,
            ]);
        }

        $this->decreaseQuantities();

        \Cart::clear();
        return redirect('checkout')->with('msg', 'Su orden fue procesada con exito');
    }

    protected function decreaseQuantities()
    {
        foreach (\Cart::getContent() as $item) {
            $article = Article::find($item->attributes->id);
            $article->update(['quantity_sold' => $article->quantity_sold + $item->quantity]);
        }
    }
}
