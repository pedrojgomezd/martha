<?php

namespace App\Http\Controllers\API\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Galery\Image;
use App\Models\Inventory\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return ['data' => $product->orderBy('id', 'DESC')->get()->load('prices', 'articles', 'provider', 'imgs', 'sub_category')];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $request->validate([
            'provider_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'bar_code' => 'required',
            'reference' => 'required',
            'initial_amount' => 'required',
            'cost' => 'required',
            'prices' => 'required',
            'articles' => 'required',
            'country' => 'required',
            'material' => 'required',
            'imgs' => 'required',
            'colors' => 'required'
        ]);

        $product = Product::create($request->all());

        foreach ($request->prices as $price => $value) {
            $product->prices()->attach([$price+1 => $value]);
        }

        $product->articles()->createMany(
                $request->articles
            );
        
        $product->colors()->createMany($request->colors);

        $product->imgs()->attach($request->imgs);

        return response()->json([], 200);
        // return $this->uploadFiles($request, $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inventory\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();
        return ['data' => $product->load('prices', 'articles', 'provider', 'imgs', 'sub_category', 'sub_category.category', 'colors')];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inventory\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if($request->colors){
            $product->colors()->create($request->colors);
            return 'color';
        }
        $product->update($request->all());
        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inventory\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {       
        $product->delete();

        return response()->json(['deleted_at' => $product->deleted_at], 200);
    }

    public function restore($id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();
        $product->restore();
        return response()->json(['msg' => 'restored'], 200);
    }

    /**
     * UploadFiles
     * @param  Request $request 
     * @return collection
     */
    protected function uploadFiles(Request $request, $product)
    {

        if ($request->hasFile('file')) {        
        
            $now = now()->format('Y-m-d');
        
            foreach ($request->file('file') as $file) {
                $name_file = $file->storeAs("public/images/inventories/{$now}", "{$now}-{$file->getClientOriginalName()}");
                
                $product->imgs()->save(new Image(['url' => str_after($name_file, 'public')]));
            }

        }

        return $product->load('imgs');
    }

    public function attachImg($id)
    {
        $product = Product::find($id);

        $product->imgs()->attach(request()->imgs);

        return response()->json(['data' => $product->imgs], 200);
    }

    public function dettachImg($id)
    {
        $product = Product::find($id);

        $product->imgs()->detach(request()->img);

        return response()->json(['data' => $product->imgs], 200);
    }

    public function addArticle(Product $product)
    {
        $data = request()->validate([
            'size' => 'required',
            'color' => 'required',
            'quantity_sold' => 'required',
            'quantity' => 'required',
        ]);
        
        $product->articles()->create($data);

        return $data;
    }

    public function updateCategory(Request $request, Product $product)
    {
        $product->update(['sub_category_id' => $request->sub_category]);

        return $product->fresh();
    }
}
