<?php

namespace App\Http\Controllers\API\Invoice;

use App\Client;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Article;
use App\SalesInvoice;
use Illuminate\Http\Request;

class SalesInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ['data' => SalesInvoice::with('client', 'articles', 'articles.product')->orderBy('id', 'DESC')->get()];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'client' => 'required',
            'created' => 'required',
            'expires' => 'required',
            'total' => 'required',
            'items' => 'required',
        ]);

        $client = Client::find($request->client);

        $invoice = $client->sales_invoices()
                          ->create($request->all());

        $invoice->addArticles($request->items);

        $this->articlesAddSale($request->items);

        return ['data' => $invoice->load('client', 'articles')];

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalesInvoice  $salesInvoice
     * @return \Illuminate\Http\Response
     */
    public function show($sale)
    {
        $salesInvoice = SalesInvoice::find($sale)->load('client', 'articles', 'articles.product');

        return ['data' => $salesInvoice];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesInvoice  $salesInvoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesInvoice $salesInvoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesInvoice  $salesInvoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesInvoice $salesInvoice)
    {
        //
    }

    private function articlesAddSale($articles)
    {
        foreach ($articles as $article) {
            $article_find = Article::find($article['id']);

            $article_find->update([
                'quantity_sold' => ($article_find->quantity_sold + $article['pivot']['quantity'])
            ]);
        }
    }
}
