<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{
    public function index()
    {
        return Order::orderBy('id', 'DESC')->with('articles', 'articles.product')->get();
    }

    public function show(Order $order)
    {
        return $order->load('articles.product.prices');
    }
}
