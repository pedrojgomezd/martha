<?php

namespace App\Http\Controllers\API\Config;

use App\Models\Config\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Category::all()->load('sub_categories'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['slug' => str_slug($request->name), 'order' => 0]);

        $category = Category::create($request->all());
        return $category->load('sub_categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Config\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Config\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Config\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
    }

    public function addSubCategory(Request $request, Category $category)
    {

        $request->request->add(['slug' => str_slug($request->name), 'order' => 0]);
        
        $sub_category = $category->sub_categories()->create($request->all());

        return $sub_category;
    }
}
