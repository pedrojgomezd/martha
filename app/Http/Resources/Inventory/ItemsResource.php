<?php

namespace App\Http\Resources\Inventory;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'reference' => $this->reference,
            'description' => $this->description,
            'price' => $this->price,
            'cost' => $this->cost,
            'quantity_sold' => (int) $this->quantity_sold,
            'initial_amount' => (int) $this->initial_amount,
            'photos' => $this->photos,
            'provider' => $this->provider,
        ];
    }
}
