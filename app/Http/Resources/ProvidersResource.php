<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProvidersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return ['id' => $this->id,
                'document' => $this->document,
                'name' => $this->name,
                'representative' => $this->representative,
                'mobil' => $this->mobil,
                'phone' => $this->phone,
                'email' => $this->email,
                'address' => $this->address
            ];
    }
}
