<?php

namespace App;

use App\Models\Inventory\InventoryItem;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = ['document', 'name', 'representative', 'email', 'mobil', 'phone', 'address'];

    public function inventory_items()
    {
    	return $this->hasMany(InventoryItem::class);
    }
}
