<?php

namespace App\Repositories;

use App\Models\Config\Category;

class Categories 
{
    public $category;

    public function __construct()
    {
        $this->category = new Category;
    }

    public function allCategoryWithSubCategory()
    {
        return $this->category->with('sub_categories')->get();
    }
}
