<?php

namespace App\Repositories;

use App\Models\Inventory\Product;

class Products
{
    public $product;

    public function __construct()
    {
        $this->product = new Product;
    }

    /**
     * @return Product
     */
    public function getProducts()
    {
        return $this->product->get();
    }

    /**
     * @return Product
     */
    public function getSuggestions()
    {
        return $this->product->limit(6)->inRandomOrder()->get();
    }

    public function getRecients()
    {
        return $this->product->orderBy('id', 'DESC')
                    ->limit(6)
                    ->get();
    }
}