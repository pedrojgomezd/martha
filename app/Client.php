<?php

namespace App;

use App\Models\Config\Price;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['price_id', 'first_name', 'last_name', 'email', 'mobil', 'address'];

    /**
     * Ralationship
     */
    
    public function sales_invoices()
    {
    	return $this->hasMany(SalesInvoice::class);
    }

    public function type_price()
    {
        return $this->belongsTo(Price::class, 'price_id');
    }
    
    /**
     * Attributtes
     */

    public function getFullNameAttribute()
    {
    	return "{$this->first_name} {$this->last_name}";
    }
}
