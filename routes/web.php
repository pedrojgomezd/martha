<?php

Route::get('/', 'Commerce\HomeController');
Route::get('/category/{category}/{sub_category?}', 'Commerce\CategoryController');
Route::get('/search', 'Commerce\SearchController');
Route::resource('products', 'Commerce\ProductController');

//Route::resource('cart', 'Commerce\CartController');
Route::get('cart/clear', function () {
    Cart::clear();
    return redirect()->route('cart.index');
});
Route::resource('cart', 'Commerce\CartController');
// Route::get('cart', 'Commerce\CartController@index')->name('cart.index');
// Route::post('cart/{product}', 'Commerce\CartController@store')->name('cart.store');
// Route::patch('cart/{product}', 'Commerce\CartController@update')->name('cart.update');
// Route::delete('cart/{product}', 'Commerce\CartController@destroy')->name('cart.destroy');

Route::resource('checkout', 'Commerce\CheckoutController');

Auth::routes();
Route::redirect('admin', 'admin/dashboard');
Route::any('/admin/{any}', function() {
    return view('layouts.template');
})->where('any', '.*')->middleware('auth');

Route::get('/api/cart', function() {
    return \Cart::getContent();
});


Route::get('/api/cart/subtotal', function() {
    return \Cart::getSubTotal();
});