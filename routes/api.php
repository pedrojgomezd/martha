<?php

use App\Models\Galery\Image;
use App\Models\Inventory\Product;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('users', 'UserController');
Route::resource('clients', 'ClientController');
Route::resource('providers', 'ProviderController');

Route::group(['prefix' => 'inventories', 'namespace' => 'Inventory'], function() {
	Route::resource('items', 'ItemController');
    Route::resource('products', 'ProductController');
    Route::post('products/{id}/restore', 'ProductController@restore');
    Route::post('products/upload/{id}', 'ProductController@attachImg');
    Route::post('products/dettachimg/{id}', 'ProductController@dettachImg');
    Route::post('products/update_category/{product}', 'ProductController@updateCategory');
    Route::post('new_article/{product}', 'ProductController@addArticle');

    Route::resource('update_article', 'ArticleController');

    Route::put('update_price/{id}', function ($id) {
        $product = Product::find(request()->pivot['product_id']);

        $product->prices()->updateExistingPivot($id, ['amount' => request()->pivot['amount']]);

        return request();
    });
});

Route::group(['prefix' => 'branch'], function() {
	Route::apiResource('/branches', 'BranchController');
});

Route::group(['prefix' => 'invoice'], function() {
	Route::resource('sales', 'Invoice\SalesInvoiceController');    
});

Route::group(['prefix' => 'commerce'], function() {
	Route::resource('orders', 'OrderController');    
});

Route::group(['prefix' => 'config', 'namespace' => 'Config'], function() {
    Route::resource('prices', 'PriceController');
    Route::resource('categories', 'CategoryController');
    Route::post('categories/addsubcategory/{category}', 'CategoryController@addSubCategory');
    Route::resource('sub_category', 'SubCategoryController');
});


Route::post('upload', function() {
	$request = request();
	$imgs = collect();

	 if ($request->hasFile('file')) {        
        
            $now = now()->format('Y-m-d');
        
            foreach ($request->file('file') as $file) {
                $name_file = $file->storeAs("public/images/inventories/{$now}", "{$now}-{$file->getClientOriginalName()}");

                $new_img = Image::create(['url' => str_after($name_file, 'public')]);

                $imgs->push($new_img->id);
            }

        }

        return $imgs->toArray();
});