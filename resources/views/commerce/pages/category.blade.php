@extends('commerce.layouts')

@section('content')
<section class="section-wrap pt-60 pb-30 catalog">
    <div class="container">
        @include('commerce.components.category.breadcrumbs')
        
        <div class="row">
            <div class="col-lg-9 order-lg-2 mb-40">
                @include('commerce.components.category.filters')
                <div class="row row-8">
                    @each('commerce.components.products.product', $category->products, 'item')
                    {{-- @include('commerce.components.products.product') --}}
                </div> 
                @include('commerce.components.category.pagination')
            </div>
            @include('commerce.components.category.sidebar')
        </div>
    </div>
</section>
@endsection