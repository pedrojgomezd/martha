@extends('commerce.layouts')

@section('content')
 <!-- Single Product -->
 <section class="section-wrap pb-20 product-single">
    <div class="container">

      <!-- Breadcrumbs -->
      <ol class="breadcrumbs">
        <li>
          <a href="index.html">Home</a>
        </li>
        <li>
          <a href="index.html">Women</a>
        </li>
        <li class="active">
          {{ $product->name }}
        </li>
      </ol>

      <div class="row">

        <div class="col-md-6 product-slider mb-50">

          <div class="flickity flickity-slider-wrap mfp-hover" id="gallery-main">
            @foreach($product->imgs as $img)
            <div class="gallery-cell">
              <a href="/storage{{ $img->url }}" class="lightbox-img">
                <img src="/storage{{ $img->url }}" alt="" />
              </a>
            </div>
            @endforeach
          </div> <!-- end gallery main -->

          <div class="gallery-thumbs" id="gallery-thumbs">
            @foreach($product->imgs as $img)
            <div class="gallery-cell">
              <img src="/storage{{ $img->url }}" alt="" />
            </div>
            @endforeach
          </div> <!-- end gallery thumbs -->

        </div> <!-- end col img slider -->

        <div class="col-md-6 product-single">
          <h1 class="product-single__title uppercase">{{ $product->name }}</h1>

          {{-- <div class="rating">
            <a href="#">3 Reviews</a>
          </div> --}}

          <span class="product-single__price">
            <ins>
              <span class="amount">${{ $product->prices[0]->pivot->amount }}</span>
            </ins>
            <del>
              {{-- <span>$30.00</span> --}}
            </del>
          </span>            

          <div class="colors clearfix">
            <span class="colors__label">Compartir 
              {{-- <span class="colors__label-selected">Fadaed Blue</span> --}}
            </span>
            <a href="whatsapp://send?text={{ url("/products/{$product->id}") }}" class="">
              <img src="https://cdn.icon-icons.com/icons2/840/PNG/512/Whatsapp_icon-icons.com_66931.png" alt="" width="50">
            </a>
            <a href="https://www.facebook.com/sharer/sharer.php?u={{ url("/products/{$product->id}") }}" class="">
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Facebook_Logo_%282019%29.png/600px-Facebook_Logo_%282019%29.png" alt="" width="38">
            </a>            
            <span class="colors__label">Colors 
              {{-- <span class="colors__label-selected">Fadaed Blue</span> --}}
            </span>
            @foreach ($product->colors as $color)
              <a href="#" class="colors__item colors__item--selected" style="background: {{ $color->hex }}"></a>
            @endforeach
          </div>
          <form-add-cart :product='{{ $product }}'></form-add-cart>
          <div class="product_meta">
            <ul>
              <li>
                <span class="product-code">Product Code:  <span>{{ $product->reference }}</span></span>
              </li>
              <li>
                <span class="product-material">Material: <span>{{ $product->material }}</span></span>
              </li>
              <li>
                <span class="product-country">Country: <span>Made in {{ $product->country }}</span></span>
              </li>
            </ul>                              
          </div>

          <!-- Accordion -->
          <div class="accordion mb-50" id="accordion">
            <div class="accordion__panel">
              <div class="accordion__heading" id="headingOne">
                <a data-toggle="collapse" href="#collapseOne" class="accordion__link accordion--is-open" aria-expanded="true" aria-controls="collapseOne">Description<span class="accordion__toggle">&nbsp;</span>
                </a>
              </div>
              <div id="collapseOne" class="collapse show" data-parent="#accordion" role="tabpanel" aria-labelledby="headingOne">
                <div class="accordion__body">
                  {{ $product->description }}
                </div>
              </div>
            </div>

            <div class="accordion__panel">
              <div class="accordion__heading" id="headingTwo">
                <a data-toggle="collapse" href="#collapseTwo" class="accordion__link accordion--is-closed" aria-expanded="false" aria-controls="collapseTwo">Information<span class="accordion__toggle">&nbsp;</span>
                </a>
              </div>
              <div id="collapseTwo" class="collapse" data-parent="#accordion" role="tabpanel" aria-labelledby="headingTwo">
                <div class="accordion__body">
                  <table class="table shop_attributes">
                    <tbody>
                      <tr>
                        <th>Size:</th>
                        <td>EU 41 (US 8), EU 42 (US 9), EU 43 (US 10), EU 45 (US 12)</td>
                      </tr>
                      <tr>
                        <th>Colors:</th>
                        <td>
                          @foreach ($product->colors as $color)
                              {{ $color->description }},
                          @endforeach
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            {{-- <div class="accordion__panel">
              <div class="accordion__heading" id="headingThree">
                <a data-toggle="collapse" href="#collapseThree" class="accordion__link accordion--is-closed" aria-expanded="false" aria-controls="collapseThree">Reviews<span class="accordion__toggle">&nbsp;</span>
                </a>
              </div>
              <div id="collapseThree" class="collapse" data-parent="#accordion" role="tabpanel" aria-labelledby="headingThree">
                <div class="accordion__body">
                  <div class="reviews">
                    <ul class="reviews__list">
                      <li class="reviews__list-item">
                        <div class="reviews__body">
                          <div class="reviews__content">
                            <p class="reviews__author"><strong>Alexander Samokhin</strong> - May 6, 2017 at 12:48 pm</p>
                            <div class="rating">
                              <a href="#"></a>
                            </div>
                            <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                          </div>
                        </div>
                      </li>

                      <li class="reviews__list-item">
                        <div class="reviews__body">
                          <div class="reviews__content">
                            <p class="reviews__author"><strong>Christopher Robins</strong> - May 7, 2014 at 12:48 pm</p>
                            <div class="rating">
                              <a href="#"></a>
                            </div>
                            <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                          </div>
                        </div>
                      </li>

                    </ul>         
                  </div> <!--  end reviews -->
                </div>
              </div>
            </div> --}}
          </div> <!-- end accordion -->

        </div> <!-- end col product description -->
      </div> <!-- end row -->
     
    </div> <!-- end container -->
  </section> <!-- end single product -->
@endsection