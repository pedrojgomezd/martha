@extends('commerce.layouts')
@section('content')
    @include('commerce.components.hero.hero')
    <div id="recient">
        @products(['products' => $recients])
        @slot('title', 'Reciente publicados')
        @endproducts
    </div>
    <div id="suggestion">
        @products(['products' => $suggestions])
        @slot('title', 'Recomendados')
        @endproducts
    </div>
@endsection