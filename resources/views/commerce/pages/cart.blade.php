@extends('commerce.layouts')

@section('content')
 <!-- Page Title -->
 <section class="page-title text-center">
    <div class="container">
      <h1 class=" heading page-title__title">checkout</h1>
    </div>
  </section> <!-- end page title -->

 @if (session('msg'))
   <div class="container">
     <div class="alert alert-success">
       {{ session('msg') }}
     </div>
   </div>
 @endif

  <!-- Checkout -->
  <section class="section-wrap checkout">
    <div class="container relative">
      <div class="row">

        <div class="ecommerce col">

          <div class="row mb-30">
            <div class="col-md-8">
              <p class="ecommerce-info">
                Returning Customer? 
                <a href="#" class="showlogin">Click here to login</a>
              </p>
            </div>
          </div>

          <form name="checkout" action="{{ route('checkout.store') }}" method="post" class="checkout ecommerce-checkout row">
            @csrf
            <div class="col-lg-7" id="customer_details">
              <div>
                <h2 class="uppercase mb-30">billing details</h2>

                <div class="row">
                  <div class="col-md-6">
                    <p class="form-row form-row-first validate-required ecommerce-invalid ecommerce-invalid-required-field" id="billing_first_name_field">
                      <label for="billing_first_name">First Name
                        <abbr class="required" title="required">*</abbr>
                      </label>
                      <input type="text" class="input-text" placeholder value name="billing_first_name" id="billing_first_name">
                    </p>
                  </div>
                  <div class="col-md-6">
                    <p class="form-row form-row-last validate-required ecommerce-invalid ecommerce-invalid-required-field" id="billing_last_name_field">
                      <label for="billing_last_name">Last Name
                        <abbr class="required" title="required">*</abbr>
                      </label>
                      <input type="text" class="input-text" placeholder value name="billing_last_name" id="billing_last_name">
                    </p>
                  </div> 
                </div> <!-- end name / last name -->

                <p class="form-row form-row-wide address-field validate-required ecommerce-invalid ecommerce-invalid-required-field" id="billing_address_1_field">
                  <label for="billing_address_1">Address
                    <abbr class="required" title="required">*</abbr>
                  </label>
                  <input type="text" class="input-text" placeholder="Street address" value name="billing_address_1" id="billing_address_1">
                </p>

                <p class="form-row form-row-wide address-field" id="billing_address_2_field">
                  <input type="text" class="input-text" placeholder="Apartment, suite, unit etc. (optional)" value name="billing_address_2" id="billing_address_2">
                </p>

                <p class="form-row form-row-wide address-field validate-required" id="billing_city_field" data-o_class="form-row form-row-wide address-field validate-required">
                  <label for="billing_city">Town / City
                    <abbr class="required" title="required">*</abbr>
                  </label>
                  <input type="text" class="input-text" placeholder="Town / City" value name="billing_city" id="billing_city">
                </p>

                <p class="form-row form-row-first address-field validate-state" id="billing_state_field" data-o_class="form-row form-row-first address-field validate-state">
                  <label for="billing_state">State / County</label>
                  <input type="text" class="input-text" placeholder value name="billing_state" id="billing_state">
                </p>

                <p class="form-row form-row-last address-field validate-required validate-postcode ecommerce-invalid ecommerce-invalid-required-field" id="billing_postcode_field" data-o_class="form-row form-row-last address-field validate-required validate-postcode">
                  <label for="billing_postcode">Postcode / ZIP
                    <abbr class="required" title="required">*</abbr>
                  </label>
                  <input type="text" class="input-text" placeholder value name="billing_postcode" id="billing_postcode">
                </p>

                <div class="row">
                  <div class="col-md-6">
                    <p class="form-row form-row-last validate-required validate-phone" id="billing_phone_field">
                      <label for="billing_phone">Phone
                        <abbr class="required" title="required">*</abbr>
                      </label>
                      <input type="text" class="input-text" placeholder value name="billing_phone" id="billing_phone">
                    </p>
                  </div>
                  <div class="col-md-6">
                    <p class="form-row form-row-first validate-required validate-email" id="billing_email_field">
                      <label for="billing_email">Email Address
                        <abbr class="required" title="required">*</abbr>
                      </label>
                      <input type="text" class="input-text" placeholder value name="billing_email" id="billing_email">
                    </p>
                  </div>
                </div>                      

              </div>

            </div> <!-- end col -->

            <!-- Your Order -->
            <div class="col-lg-5">
              <div class="order-review-wrap ecommerce-checkout-review-order" id="order_review">
                <h2 class="uppercase">Your Order</h2>
                <table class="table shop_table ecommerce-checkout-review-order-table">
                  <tbody>
                    @foreach(Cart::getContent() as $cart)
                    <tr>
                      <th>{{ $cart->name }}<span class="count"> x {{ $cart->quantity }}</span></th>
                      <td>
                        <span class="amount">${{ $cart->price * $cart->quantity }}</span>
                      </td>
                    </tr>
                    @endforeach

                    <tr class="cart-subtotal">
                      <th>Cart Subtotal</th>
                      <td>
                        <span class="amount">${{ Cart::getSubTotal() }}</span>
                      </td>
                    </tr>
                    <tr class="shipping">
                      <th>Shipping</th>
                      <td>
                        <span>Free Shipping</span>
                      </td>
                    </tr>
                    <tr class="order-total">
                      <th><strong>Total Amount</strong></th>
                      <td>
                        <strong><span class="amount">${{ Cart::getTotal() }}</span></strong>
                      </td>
                    </tr>
                  </tbody>
                </table>

                <div id="payment" class="ecommerce-checkout-payment">
                  <h2 class="uppercase">Payment Method</h2>
                  <ul class="payment_methods methods">

                    <li class="payment_method_bacs">
                      <input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="bacs" checked="checked">
                      <label for="payment_method_bacs">Credit Card</label>
                      <div class="payment_box payment_method_bacs">
                        formulario metodo de pago
                      </div>
                    </li>

                  </ul>
                  <div class="form-row place-order">
                    <input type="submit" name="ecommerce_checkout_place_order" class="btn btn-lg btn-color btn-button" id="place_order" value="Place order">
                  </div>
                </div>
              </div>
            </div> <!-- end order review -->
          </form>

        </div> <!-- end ecommerce -->

      </div> <!-- end row -->
    </div> <!-- end container -->
  </section> <!-- end checkout -->
@endsection