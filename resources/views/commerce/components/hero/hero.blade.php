<!-- Hero Slider -->
<section class="hero">
    <div id="owl-hero" class="owl-carousel owl-theme owl-carousel--dots-inside">        

    <div class="hero__slide" style="background-image: url(/commerce/img/hero/adnomen.jpg)">
        <div class="container realative">
            <div class="hero__holder-1">
                <h1 class="hero__title-1">Think of you! It is the way.</h1>
                <a href="#suggestion" class="hero__link-1 btn btn-lg btn-dark">View more.</a>
            </div>
        </div>          
    </div>
{{-- 
    <div class="hero__slide" style="background-image: url(/commerce/img/hero/2.jpg)">
        <div class="container relative">
        <div class="hero__holder">
            <h1 class="hero__title-1">dope<br>street<br>wear</h1>
            <a href="single-product.html" class="hero__link-1 btn btn-lg btn-dark"><span>New Arrivals</span></a>
        </div>
        </div>
    </div>

    <div class="hero__slide" style="background-image: url(/commerce/img/hero/3.jpg)">
        <div class="container text-center">
        <div class="hero__holder-1">
            <h1 class="hero__title-2">new lookbook</h1>
            <h3 class="hero__subtitle">Sale 50% off. Get only trendy items</h3>
            <a href="single-product.html" class="hero__link-1 btn btn-lg btn-color"><span>Shop the trend</span></a>
        </div>
        </div>
    </div> --}}

    </div> <!-- end owl -->
</section> <!-- end hero slider -->