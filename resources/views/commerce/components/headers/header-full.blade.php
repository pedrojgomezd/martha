
    <!-- Navigation -->
    <header class="nav">

      <div class="nav__holder nav--sticky">
        <div class="container relative">

          <!-- Top Bar -->
          <div class="top-bar d-none d-lg-flex">

            <!-- Currency / Language -->
            <ul class="top-bar__currency-language">
              <li class="top-bar__language">
                <a href="#">English</a>
                <div class="top-bar__language-dropdown">
                  <ul>
                    <li><a href="#">English</a></li>
                    <li><a href="#">Spanish</a></li>
                  </ul>
                </div>
              </li>
              <li class="top-bar__currency">
                <a href="#">USD</a>
                <div class="top-bar__currency-dropdown">
                  <ul>
                    <li><a href="#">USD</a></li>
                  </ul>
                </div>
              </li>              
            </ul>

            <!-- Promo -->
            {{-- <p class="top-bar__promo text-center">Free shipping on orders over $50</p> --}}

            <!-- Sign in / Wishlist / Cart -->
            <div class="top-bar__right">

              <!-- Sign In -->
              <a href="login.html" class="top-bar__item top-bar__sign-in" id="top-bar__sign-in"><i class="ui-user"></i>Sign In</a>

              <!-- Wishlist -->
              <a href="#" class="top-bar__item"><i class="ui-heart"></i></a>

             <mini-cart></mini-cart>
            </div>

          </div> <!-- end top bar -->

          <div class="flex-parent">

            <!-- Mobile Menu Button -->
            <button class="nav-icon-toggle" id="nav-icon-toggle" aria-label="Open mobile menu">
              <span class="nav-icon-toggle__box">
                <span class="nav-icon-toggle__inner"></span>
              </span>
            </button> <!-- end mobile menu button -->

            <!-- Logo -->
            <a href="/" class="logo">
              <img class="logo__img" src="/commerce/img/logo-the-girdle-girl-2.png" alt="logo">
              {{-- The Girdle Girl --}}
            </a>

            <!-- Nav-wrap -->
            <nav class="flex-child nav__wrap d-none d-lg-block">              
              <ul class="nav__menu">
                @foreach($categories as $category)
                <li class="nav__dropdown active">
                  <a href="/category/{{ $category->slug }}">{{ $category->name }}</a>
                  <ul class="nav__dropdown-menu">
                    @foreach($category->sub_categories as $sub_category)
                    <li><a href="/category/{{ $category->slug }}/{{ $sub_category->slug }}">{{ $sub_category->name }}</a></li>
                    @endforeach
                  </ul>
                </li>
                @endforeach

              </ul> <!-- end menu -->

            </nav> <!-- end nav-wrap -->


            <!-- Search -->
            <div class="flex-child nav__search d-none d-lg-block">
              <form method="get" class="nav__search-form" action='/search'>
                <input type="search" class="nav__search-input"  name='query' placeholder="Search">
                <button type="submit" class="nav__search-submit">
                  <i class="ui-search"></i>
                </button>
              </form>
            </div>


            <!-- Mobile Wishlist -->
            <a href="#" class="nav__mobile-wishlist d-lg-none" aria-label="Mobile wishlist">
              <i class="ui-heart"></i>
            </a>

            <!-- Mobile Cart -->
            {{--<a href="cart.html" class="nav__mobile-cart d-lg-none">--}}
              {{--<i class="ui-bag"></i>--}}
              {{--<span class="nav__mobile-cart-amount">(2)</span>--}}
            {{--</a>--}}
            <mini-cart-mobil></mini-cart-mobil>

            
         
        
          </div> <!-- end flex-parent -->
        </div> <!-- end container -->

      </div>
    </header> <!-- end navigation -->
