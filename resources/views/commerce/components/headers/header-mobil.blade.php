
  <!-- Mobile Sidenav -->    
  <header class="sidenav" id="sidenav">
    <!-- Search -->
    <div class="sidenav__search-mobile">
      <form method="get" class="sidenav__search-mobile-form">
        <input type="search" class="sidenav__search-mobile-input" placeholder="Search..." aria-label="Search input">
        <button type="submit" class="sidenav__search-mobile-submit" aria-label="Submit search">
          <i class="ui-search"></i>
        </button>
      </form>
    </div>

    <nav>
      <ul class="sidenav__menu" role="menubar">
        @foreach($categories as $category)
        <li>
          <a href="/category/{{ $category->slug }}" class="sidenav__menu-link">{{ $category->name }}</a>
          <button class="sidenav__menu-toggle" aria-haspopup="true" aria-label="Open dropdown"><i class="ui-arrow-down"></i></button>
          <ul class="sidenav__menu-dropdown">
            @foreach($category->sub_categories as $sub_category)
            <li><a href="/category/{{ $category->slug }}/{{ $sub_category->slug }}" class="sidenav__menu-link">{{ $sub_category->name }}</a></li>
            @endforeach
          </ul>
        </li>
        @endforeach

        <li>
          <a href="#" class="sidenav__menu-link">Sign In</a>
        </li>
      </ul>
    </nav>
  </header> <!-- end mobile sidenav -->
