 <!-- Filter -->          
 <div class="shop-filter">
    <p class="woocommerce-result-count">
        Showing: 1-12 of 80 results
    </p>
    <span class="woocommerce-ordering-label">Sort by</span>
    <form class="woocommerce-ordering">
        <select>
        <option value="default-sorting">Default Sorting</option>
        <option value="price-low-to-high">Price: high to low</option>
        <option value="price-high-to-low">Price: low to high</option>
        <option value="by-popularity">By Popularity</option>
        <option value="date">By Newness</option>
        <option value="rating">By Rating</option>
        </select>
    </form>
</div>