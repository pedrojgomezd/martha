<!-- Sidebar -->
<aside class="col-lg-3 sidebar left-sidebar">

        <!-- Categories -->
        <div class="widget widget_categories widget--bottom-line">
          <h4 class="widget-title">Categories</h4>
          <ul>
            @foreach($categories as $category)
            <li>
              <a href="/category/{{ $category->slug }}">{{ $category->name }}</a>
            </li>
            @endforeach
          </ul>
        </div>

        <!-- Size -->
        <div class="widget widget__filter-by-size widget--bottom-line">
          <h4 class="widget-title">Size</h4>
          <ul class="size-select">
            <li>
              <input type="checkbox" class="checkbox" id="small-size" name="small-size">
              <label for="small-size" class="checkbox-label">X-Small</label>
            </li>                
            <li>
              <input type="checkbox" class="checkbox" id="medium-size" name="medium-size">
              <label for="medium-size" class="checkbox-label">Small</label>
            </li>
            <li>
              <input type="checkbox" class="checkbox" id="large-size" name="large-size">
              <label for="large-size" class="checkbox-label">Meduim</label>
            </li>
            <li>
              <input type="checkbox" class="checkbox" id="xlarge-size" name="xlarge-size">
              <label for="xlarge-size" class="checkbox-label">Large</label>
            </li>
            <li>
              <input type="checkbox" class="checkbox" id="xxlarge-size" name="xxlarge-size">
              <label for="xxlarge-size" class="checkbox-label">X-Large</label>
            </li>
          </ul>
        </div>

        <!-- Color -->
        <div class="widget widget__filter-by-color widget--bottom-line">
          <h4 class="widget-title">Color</h4>
          <ul class="color-select">
            <li>
              <input type="checkbox" class="checkbox" id="green-color" name="green-color">
              <label for="green-color" class="checkbox-label">Green</label>
            </li>
            <li>
              <input type="checkbox" class="checkbox" id="red-color" name="red-color">
              <label for="red-color" class="checkbox-label">Red</label>
            </li>
            <li>
              <input type="checkbox" class="checkbox" id="blue-color" name="blue-color">
              <label for="blue-color" class="checkbox-label">Blue</label>
            </li>
            <li>
              <input type="checkbox" class="checkbox" id="white-color" name="white-color">
              <label for="white-color" class="checkbox-label">White</label>
            </li>
            <li>
              <input type="checkbox" class="checkbox" id="black-color" name="black-color">
              <label for="black-color" class="checkbox-label">Black</label>
            </li>
          </ul>
        </div>

        <!-- Filter by Price -->
        <div class="widget widget__filter-by-price widget--bottom-line">
          <h4 class="widget-title">Filter by Price</h4>
           
          <div id="slider-range"></div>
          <p>
            <label for="amount">Price:</label>
            <input type="text" id="amount">
            <a href="#" class="btn btn-sm btn-dark"><span>Filter</span></a>
          </p>
        </div>

      </aside> <!-- end sidebar -->