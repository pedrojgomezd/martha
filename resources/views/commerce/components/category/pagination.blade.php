<!-- Pagination -->
<div class="pagination clearfix">                
    <nav class="pagination__nav right clearfix">
        <a href="#" class="pagination__page"><i class="ui-arrow-left"></i></a>
        <span class="pagination__page pagination__page--current">1</span>
        <a href="#" class="pagination__page">2</a>
        <a href="#" class="pagination__page">3</a>
        <a href="#" class="pagination__page">4</a>
        <a href="#" class="pagination__page"><i class="ui-arrow-right"></i></a>
    </nav>
</div>