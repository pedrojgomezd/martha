<div class="col-lg-2 col-sm-4 product">
    <div class="product__img-holder">
        <a href="/products/{{ $item->id }}" class="product__link" aria-label="Product">
            <img src="/storage{{ $item->imgs[0]->url }}" alt="" class="product__img">
            <img src="/storage{{ $item->imgs[0]->url }}" alt="" class="product__img-back">
        </a>
        <div class="product__actions">
            <a href="quickview.html" class="product__quickview">
                <i class="ui-eye"></i>
                <span>Quick View</span>
            </a>
            <a href="#" class="product__add-to-wishlist">
                <i class="ui-heart"></i>
                <span>Wishlist</span>
            </a>
        </div>
    </div>

    <div class="product__details">
        <h3 class="product__title">
            <a href="/products/{{ $item->id }}">{{ $item['name'] }}</a>
        </h3>
    </div>

    <span class="product__price">
        <ins>
            <span class="amount">$ {{ $item->prices[0]->pivot->amount }}</span>
        </ins>
        <del>
            {{-- <span>$27.00</span> --}}
        </del>
    </span>
</div> <!-- end product -->
