<!-- Best Seller -->
<section class="section-wrap pb-30">
    <div class="container">

        <div class="heading-row">
            <div class="text-center">
                <h2 class="heading bottom-line"> {{ $title }}</h2>
            </div>
        </div>

        <div class="row row-8">
            @each('commerce.components.products.product', $products, 'item')
        </div> <!-- end row -->
    </div> <!-- end container -->
</section> <!-- end best seller -->
