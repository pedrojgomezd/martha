<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>The Girdle Girl</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Questrial:400%7CMontserrat:300,400,700,700i' rel='stylesheet'>
    <link rel="icon" type="image/png" href="/commerce/img/logo-the-girdle-girl-2.png" />

    <!-- Css -->
    <link rel="stylesheet" href="/commerce/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/commerce/css/font-icons.css" />
    <link rel="stylesheet" href="/commerce/css/style.css" />
    <link rel="stylesheet" href="/commerce/css/color.css" />
    <meta property="og:url"                content="{{ isset($product) ? url("/products/{$product->id}") : '' }}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="{{ isset($product) ? $product->name : '' }}" />
    <meta property="og:description"        content="{{ isset($product) ? $product->description : '' }}" />
    <meta property="og:image" content="{{ isset($product) ? url("/storage{$product->imgs[0]->url}") : '' }}"/>
</head>
<body>

<div id="app">    
    <!-- Preloader -->
    <div class="loader-mask">
        <div class="loader">
            <div>
                <img src="/commerce/img/logo-the-girdle-girl.png" alt="">
            </div>
        </div>
    </div>
    @include('commerce.components.headers.header-mobil')
    <main class='main oh' id='main'>
        @include('commerce.components.headers.header-full')
        
        @yield('content')
        
        @include('commerce.components.footer.footer')
    </main>
</div>
    <!-- jQuery Scripts -->
    <script src="/js/commerce.js"></script>
    <script type="text/javascript" src="/commerce/js/jquery.min.js"></script>
    <script type="text/javascript" src="/commerce/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/commerce/js/easing.min.js"></script>
    <script type="text/javascript" src="/commerce/js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="/commerce/js/owl-carousel.min.js"></script>  
    <script type="text/javascript" src="/commerce/js/flickity.pkgd.min.js"></script>
    <script type="text/javascript" src="/commerce/js/modernizr.min.js"></script>
    <script type="text/javascript" src="/commerce/js/scripts.js"></script>
</body>
</html>