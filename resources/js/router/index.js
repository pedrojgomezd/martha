import VueRouter from 'vue-router'
import routes from './routes'

const router = new VueRouter({
	mode: 'history',
	base: '/',
	linkActiveClass: 'active',
	routes
})
/*
router.beforeResolve((to, from, next) => {
	One.layout('header_loader_on')
	next()
})

router.afterEach((to, from) => {
  One.layout('header_loader_off')
})
*/
export default router
