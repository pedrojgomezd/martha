import Dashboard from './../pages/Dashboard'

const routes = [
	{
		path: '/admin/dashboard',
		name: 'DASHBOARD',
		icon: 'si si-speedometer',
		needAdmin: false,
		component: Dashboard
	},
	{
		path: '/admin/invoices',
		name: 'INGRESOS',
		icon: 'fa fa-hand-holding-usd',
		needAdmin: false,
		component: require('./../pages/invoices/InvoiceIndexPage'),
		children: [
			{
				needAdmin: false,
				path: '/admin/invoices/',
				name: 'Facturas de venta',
				component: require('./../pages/invoices/InvoiceListPage')
			},
			{
				path: '/admin/invoices/create',
				no_show: true,
				needAdmin: false,
				name: 'Facturas de venta',
				component: require('./../pages/invoices/InvoiceCreatePage')
			},
			{
				path: '/admin/invoices/sale/:invoice',
				name: 'Facturas de venta',
				no_show: true,
				needAdmin: false,
				component: require('./../pages/invoices/InvoiceDetailsPage')
			},
		]
	},
	{
		path: '/admin/clients/',
		name: 'CONTACTOS',
		icon: 'fa fa-address-book',
		needAdmin: false,
		component: require('./../pages/contacts/ContactIndexPage'),
		children: [
			{
				needAdmin: false,
				path: "/admin/clients",
				name: 'Clientes',
				component: require('./../pages/contacts/clients/ListClientPage')
			},
			{
				path: "/admin/clients/create",
				name: 'Crear',
				no_show: true,
				needAdmin: false,
				component: require('./../pages/contacts/clients/CreatePage')
			},
			{
				path: '/admin/clients/:client',
				name: 'Edit',
				no_show: true,
				needAdmin: true,
				component: require('./../pages/contacts/clients/ClientShowPage')
			},
			{
				path: '/admin/clients/:client/edit',
				name: 'Edit',
				no_show: true,
				needAdmin: false,
				component: require('./../pages/contacts/clients/ClientEditPage')
			},
			{
				needAdmin: true,
				path: "/admin/providers",
				name: 'Proveedores',
				component: require('./../pages/contacts/providers/ProviderListPage')
			},
			{
				path: "/admin/providers/create",
				name: 'Crear',
				no_show: true,
				needAdmin: true,
				component: require('./../pages/contacts/providers/ProviderCreatePage')
			},
			{
				path: '/admin/providers/:provider',
				name: 'Edit',
				no_show: true,
				needAdmin: true,
				component: require('./../pages/contacts/providers/ProviderEditPage')
			}
		]
	},
	{
		path: '/admin/inventories/',
		name: 'INVENTARIO',
		icon: 'fa fa-barcode',
		needAdmin: false,
		component: require('./../pages/inventory/InventoryIndexPage'),
		children: [
			{
				needAdmin: false,
				path: '/admin/inventories',
				name: 'Lista de articulos',
				component: require('./../pages/inventory/products/ProductListPage'),	
			},
			{
				needAdmin: true,
				path: '/admin/inventories/create',
				name: 'Agregar Item',
				component: require('./../pages/inventory/products/ProductCreatePage'),
			},
			{
					path: '/admin/inventories/items/:item',
					name: 'Detalles Item',
					no_show: true,
					needAdmin: false,
					component: require('./../pages/inventory/products/ProductShowPage'),
			}
		]
	},
	{
		path: '/admin/branches/',
		name: 'SUCURSALES',
		icon: 'fa fa-barcode',
		needAdmin: true,
		component: require('./../pages/branches/BranchIndexPage'),
		children: [
			{
				needAdmin: false,
				path: '/admin/branches',
				name: 'Listar',
				component: require('./../pages/branches/BranchListPage'),	
			},
			{
				needAdmin: false,
				path: '/admin/branches/create',
				name: 'Crear',
				component: require('./../pages/branches/BranchCreatePage'),
			},
			{
					path: '/admin/branches/items/:item',
					name: 'Detalles Item',
					no_show: true,
					needAdmin: false,
					component: require('./../pages/inventory/products/ProductShowPage'),
			}
		]
	},	
	{
		path: '/admin/commerce/',
		name: 'COMMERCE',
		icon: 'fa fa-barcode',
		needAdmin: true,
		component: require('./../pages/commerce/CommerceIndexPage'),
		children: [
			{
				needAdmin: false,
				path: '/admin/orders',
				name: 'Ordenes',
				component: require('./../pages/commerce/orders/OrderListPage'),
			},
			{
				path: '/admin/orders/:order',
				name: 'Ordenes',
				no_show: true,
				needAdmin: false,
				component: require('./../pages/commerce/orders/ShowOrderPage'),
			}
		]
	},
	{
		path: '/admin/acounts',
		name: 'BANCOS',
		icon: 'fa fa-building',
		needAdmin: true,
		component: require('./../pages/acounts/AcountIndexPage'),
		children: [
			{
				needAdmin: false,
				path: '/admin/acounts',
				name: 'Ver bancos',
				component: require('./../pages/acounts/AcountListPage'),
			},
			{
				path: '/admin/acounts/create',
				name: 'Banco',
				no_show: true,
				needAdmin: false,
				component: require('./../pages/acounts/AcountCreatePage'),
			}
		]
	},
	{
		path: '/admin/users',
		name: 'USUARIOS',
		icon: 'fa fa-users',
		needAdmin: true,
		component: require('./../pages/users/UserIndexPage'),
		children: [
			{
				needAdmin: false,
				path: '/admin/users',
				name: 'Lista',
				component: require('./../pages/users/UserListPage')
			},
			{
				needAdmin: false,
				path: '/admin/users/create',
				name: 'Crear',
				component: require('./../pages/users/UserCreatePage')
			},
			{
				path: '/admin/users/:user',
				name: 'Edit',
				no_show: true,
				needAdmin: false,
				component: require('./../pages/users/UserEditPage')
			}
		]
	},
	{
		path: '/admin/config',
		name: 'CONFIGURACIONES',
		icon: 'fa fa-load',
		needAdmin: true,
		component: require('./../pages/config/ConfigIndexPage'),
		children: [
			{
				needAdmin: false,
				path: '/admin/prices',
				name: 'Precios',
				component: require('./../pages/config/prices/ConfigPriceList.vue')
			},
			{
				needAdmin: false,
				path: '/admin/categories',
				name: 'Categorias',
				component: require('./../pages/config/categories/ConfigCategoryList.vue')
			}
		]
	}
]

export default routes
