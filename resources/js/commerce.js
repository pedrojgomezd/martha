require('./bootstrap');

 
window.Vue = require('vue');
/* Complement */
import VueSweetalert2 from 'vue-sweetalert2';
import FormAddCart from './commerce/form/FormAddCart';
import MiniCart from './commerce/cart/MiniCart';
import MiniCartMobil from './commerce/cart/MiniCartMobil';

const app = new Vue({
    el: '#app',
    components: { FormAddCart, MiniCart, MiniCartMobil }
});
