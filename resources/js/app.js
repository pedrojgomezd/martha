require('./bootstrap');

 
window.Vue = require('vue');
/* Complement */
import VueSweetalert2 from 'vue-sweetalert2';

import store from './store'
import VueRouter from 'vue-router';
import router from './router'

//import App from './components/AppComponent'
import Breadcrumbs from './components/widgets/Breadcrumbs'

Vue.use(VueSweetalert2);
Vue.use(VueRouter);

Vue.prototype.router = router

Vue.component('app', require('./layouts/Template'))
Vue.component('content-component', require('./components/AppComponent'))

const app = new Vue({
    el: '#app',
    created () {
    	this.$store.commit('userLogin/setUser', window.Laravel.user);
    },
    components: { Breadcrumbs },
    router,
    store
});
