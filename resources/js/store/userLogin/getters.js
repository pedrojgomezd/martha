export default {
    getUser(state) {
        return state.user
    },
    userIsAdmin(state) {
        return state.user.type == 'admin' ? true : false
    }
}