import Axios from "axios";

export default {
    getOrders: ({commit}) => {
        return new Promise((resolver, reject) => {
            axios.get('/api/commerce/orders')
                .then(({data}) => {
                    resolver(data)
                    commit('pushOrders', data)
                })
        })
    },

    getOrder: ({commit}, order) => {
		axios.get(`/api/commerce/orders/${order}`).then(({data}) => {
			commit('showOrder', data)
		})
	},
}