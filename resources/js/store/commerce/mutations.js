export default {
	pushOrders: (state, orders) => {
		state.orders = orders
		state.loaded = true
    },
    
    showOrder: (state, order) => {
        state.order = order
    }
}