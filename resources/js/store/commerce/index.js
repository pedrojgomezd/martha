import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {
	orders: [],
	order: {},
	loaded: false
}

export default {
	namespaced: true,
	actions,
	state,
	getters,
	mutations	
}