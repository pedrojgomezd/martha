export default {
	getUsers: ({commit}) => {
		return new Promise( (resolver, reject ) => {
			axios.get('/api/users').then(({data}) => {
				commit('setUsers', data.data)
				resolver()
			})
		})
	},

	setUser: ({commit}, user) => {
		return new Promise( (resolver, reject) => {
			axios.post('/api/users', user).then(({data}) => {
				commit('setUser', data.data)
				resolver()
			}).catch( ({response}) => {
				reject(response.data)
			})
		})
	},

	editUser: (context, id) => {
		return new Promise( (resolver, reject) => {
			axios.get(`/api/users/${id}`).then( ({data}) => {
				context.commit('editUser', data.data)
				resolver(data)
			}).catch( ({response}) => {
				reject(response.data)
			})			
		})
	},

	updateUser: ({commit}, user) => {
		return new Promise( (resolver, reject) => {
			axios.put(`/api/users/${user.id}`, user).then(({data}) =>{
				//commit('updateUser', data.data)
				resolver()
			}).catch(({response}) => reject(response.data))
		})
	}
}