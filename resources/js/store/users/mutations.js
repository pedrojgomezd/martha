export default {
	setUsers: (state, users) => {
		state.users = users
		state.loaded = true
	},

	setUser: (state, user) => {
		state.users.push(user)
	},

	editUser: (state, user) => {
		state.user = user
	}
}