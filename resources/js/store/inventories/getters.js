export default {
	allArticles: state => {
		return state.articles
	},

	showArticle: (state, getters) => {
		return state.article
	},

	allProducts: state => {
		return state.products
	},

	showProduct: (state, getters) => {
		return state.product
	},

	countArticles: (state) => {
		let articles = state.product.articles
		
		if(!articles) {
			return 
		}
		
		return Object.keys(articles).reduce((suma, article) => {
			return articles[article].quantity + suma
		},0)
	},

	countSoldArticles: (state) => {
		let articles = state.product.articles
		
		if(!articles) {
			return 
		}

		return Object.keys(articles).reduce((suma, article) => {
			return articles[article].quantity_sold + suma
		},0)
	}
}