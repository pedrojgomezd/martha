export default {
	getArticles: ({commit}) => {
		return new Promise((resolver, reject) => {			
			axios.get('/api/inventories/items').then(({data}) => {
				commit('setArticles', data)
			}).catch( ({response}) => {
				reject(response.data)
			})
		})
	},

	showArticle: ({commit}, article) => {
		return new Promise( (resolver, reject) => {
				axios.get(`/api/inventories/items/${article}`)
					.then(({data}) => {
						commit('setArticle', data)
						resolver(data)
					})
			})
	},

	getProducts: ({commit}) => {
		return new Promise((resolver, reject) => {
			axios.get('/api/inventories/products').then(({data}) => {
				commit('setProducts', data.data)
				resolver(data.data)
			})
		})
	},

	showProduct: ({commit}, article) => {
		return new Promise( (resolver, reject) => {
				axios.get(`/api/inventories/products/${article}`)
					.then(({data}) => {
						commit('setProduct', data.data)
						resolver(data)
					})
			})
	},
}