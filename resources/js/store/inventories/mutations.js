export default {
	setArticles: (state, articles) => {
		state.articles = articles
		state.loaded = true
	},

	setArticle: (state, article) => {
		state.article = article
	},

	pushArticle: (state, article) => {
		state.articles.push(article)
	},

	setProducts: (state, products) => {
		state.products = products
		state.loaded = true
	},

	setProduct: (state, product) => {
		state.product = product
	},
}