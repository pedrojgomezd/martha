
export default {
	getPrices: ({commit}) => {
		return new Promise((resolver, reject) => {			
			axios.get('/api/config/prices').then(({data}) => {
				commit('setPrices', data)
				resolver(data)
			}).catch( ({response}) => {
				reject(response.data)
			})
		})
	},

	pushPrice: ({commit}, attributes) => {
		return new Promise((resolver, reject) => {
			axios.post('/api/config/prices', attributes).then(({data}) => {
				resolver(data)
				commit('pushPrice', data.data)
			}).catch(response => reject(response))
		}) 
	},

	updatePrice: ({commit}, attribute) => {

		return new Promise((resolver, reject) => {
			axios.put(`/api/config/prices/${attribute.id}`, attribute.data).then(({data}) => {
				resolver(data)
				commit('updatePrice', {price_id: attribute.id, data: data.data})
			}).catch(response => {
				reject(response)
			})
		})

	},

	getCategories: ({commit}) => {
		return new Promise((resolver, reject) => {			
			axios.get('/api/config/categories').then(({data}) => {
				commit('setCategories', data)
				resolver(data)
			}).catch( ({response}) => {
				reject(response.data)
			})
		})
	},

	pushCategory: ({commit}, attributes) => {
		return new Promise((resolver, reject) => {
			axios.post('/api/config/categories', attributes).then(({data}) => {
				resolver(data)
				commit('pushCategory', data)
			}).catch(response => reject(response))
		}) 
	},

	addSubCategory: ({commit}, attributes) => {
		return new Promise((resolver, reject) => {
			axios.post(`/api/config/categories/addsubcategory/${attributes.id}`, attributes).then(({data}) => {
				resolver(data)
				console.log(data)
				// commit('pushCategory', data)
			}).catch(response => reject(response))
		}) 
	}
}