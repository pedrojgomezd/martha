export default {
	allPrices: state => {
		return state.prices
	},

	allCategories: state => {
		return state.categories
	}
}