import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {
	providers: [],
	provider: {},
	loaded: false
}

export default {
	namespaced: true,
	actions,
	state,
	getters,
	mutations	
}