export default {
	getBranches: ({commit}) => {
		return new Promise( (resolver, reject ) => {
			axios.get('/api/branch/branches').then(({data}) => {
				commit('setBranches', data.data)
				resolver()
			})
		})
	},

	setBranch: ({commit}, branch) => {
		return new Promise( (resolver, reject) => {
			axios.post('/api/branch/branches', branch).then(({data}) => {
				// commit('set', data.data)
				resolver(data)
			}).catch( ({response}) => {
				reject(response.data)
			})
		})
	},

	// editUser: (context, id) => {
	// 	return new Promise( (resolver, reject) => {
	// 		axios.get(`/api/users/${id}`).then( ({data}) => {
	// 			context.commit('editUser', data.data)
	// 			resolver(data)
	// 		}).catch( ({response}) => {
	// 			reject(response.data)
	// 		})			
	// 	})
	// },

	// updateUser: ({commit}, user) => {
	// 	return new Promise( (resolver, reject) => {
	// 		axios.put(`/api/users/${user.id}`, user).then(({data}) =>{
	// 			//commit('updateUser', data.data)
	// 			resolver()
	// 		}).catch(({response}) => reject(response.data))
	// 	})
	// }
}