export default {
	getSales: ({commit}) => {
		axios.get('/api/invoice/sales').then(({data}) => {
			commit('setSales', data.data)				
		})
	},

	getSale: ({commit}, sale) => {
		axios.get(`/api/invoice/sales/${sale}`).then(({data}) => {
			commit('showSale', data.data)
		})
	},

	setSale: ({commit}, sale) => {
		return new Promise( (resolve, reject ) => {
			axios.post('/api/invoice/sales', sale)
				.then(({data}) => {
					commit('showSale', data.data)
					resolve(data.data)
				})
				.catch(({response}) => reject(response.data))
		})
	}
}