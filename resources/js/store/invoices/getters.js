export default {
	allSales: state => {
		return state.sales
	},

	lastSale: state => {
		return state.sales[0]
	},

	showSale: (state, getters) => {
		return state.sale
	}
}