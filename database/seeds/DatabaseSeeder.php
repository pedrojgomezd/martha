<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\User::class)->create([
            'first_name' => 'Pedro',
        	'last_name' => 'Gomez',
        	'email' => 'pedrojgomezd@gmail.com'
        ]);

        factory(App\Client::class, 10)->create();
        factory(App\Provider::class, 10)->create();
    }
}
