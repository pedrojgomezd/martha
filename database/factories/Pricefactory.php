<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Config\Price::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->name,
    ];
});
