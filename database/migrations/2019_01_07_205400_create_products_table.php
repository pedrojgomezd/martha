<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('provider_id')->unsigned();
            
            $table->string('name');
            $table->string('bar_code')->default('0000000');
            $table->string('description');
            $table->string('reference');
            $table->integer('initial_amount')->default(0);
            $table->integer('quantity_sold')->default(0);
            $table->decimal('cost', 4, 2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
