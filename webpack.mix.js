const mix = require('laravel-mix');

mix
	/* CSS */
    .sass('resources/sass/main.scss', 'public/css/oneui.css')
    .sass('resources/sass/oneui/themes/amethyst.scss', 'public/css/themes/')
    .sass('resources/sass/oneui/themes/city.scss', 'public/css/themes/')
    .sass('resources/sass/oneui/themes/flat.scss', 'public/css/themes/')
    .sass('resources/sass/oneui/themes/modern.scss', 'public/css/themes/')
    .sass('resources/sass/oneui/themes/smooth.scss', 'public/css/themes/')

    /* JS */
	.js('resources/js/app.js', 'public/js/app2.js')
    .js('resources/js/oneui/app.js', 'public/js/oneui.app.js')
    .js('resources/js/commerce.js', 'public/js/commerce.js')
        /* Tools */
    //.browserSync('enish.test')
    //.disableNotifications()

    /* Options */
    .options({
        processCssUrls: false
    });
